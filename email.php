<?php
// Importar as classes 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Carregar o autoloader do composer
require 'vendor/autoload.php';
// Instância da classe

$entrada = $_POST['entrada'];
$saida = $_POST['saida'];
$adulto = $_POST['adulto'];
$crianca = $_POST['crianca'];

$mail = new PHPMailer(true);
try
{
    // Configurações do servidor
    $mail->isSMTP();        //Devine o uso de SMTP no envio
    $mail->SMTPAuth = true; //Habilita a autenticação SMTP
    $mail->Username   = 'mixx.viagens@gmail.com';
    $mail->Password   = 'mix$123$456$789';
    // Criptografia do envio SSL também é aceito
    $mail->SMTPSecure = 'tls';
    // Informações específicadas pelo Google
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    // Define o remetente
    $mail->setFrom('mixx.viagens@gmail.com', 'Mix Viagens');
    // Define o destinatário
    $mail->addAddress('victor@mixinternet.com.br', 'Joao Victor');
    // Conteúdo da mensagem
    $mail->isHTML(true);  // Seta o formato do e-mail para aceitar conteúdo HTML
    $mail->Subject = 'Teste';
    $mail->Body    = "Nova reserva efetuada!<br/>
                    <p><b>Entrada:</b> {$entrada}</p>
                    <p><b>Saida:</b> {$saida}</p>
                    <p><b>Adultos:</b> {$adulto}</p>
                    <p><b>Criancas:</b> {$crianca}</p>";
    $mail->AltBody = 'Este é o cortpo da mensagem para clientes de e-mail que não reconhecem HTML';
    // Enviar
    $mail->send();
    echo 'A mensagem foi enviada!';
}
catch (Exception $e)
{
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}